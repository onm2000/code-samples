import xray
import numpy as np
from matplotlib import pyplot as plt

class time_series:
	“””Object contains time series of input variable with given parameters”””
    def __init__(self, function, varname = 'SALT', ibounds = [0, 384], jbounds = [0, 320], kbounds = [0, 60],
                years = [100, 110], months = [1, 13],
                ddir = '/data/scratch/rpa/cesm/b40.1850.track1.2deg.wcm.007/ocn-hist/b40.1850.track1.2deg.wcm.007.pop.h.0'):
        self.varname = varname
        self.ibounds = ibounds
        self.jbounds = jbounds
        self.kbounds = kbounds
        self.years = years
        self.months = months
        self.ddir = ddir
        self.params = [self.varname, self.ibounds, self.jbounds, self.kbounds]
        self.function = function
        self.out = self.create_series()
       
    def create_series(self):
	“”” Creates time series of the input variable”””
        out = np.array(())
        for year in xrange(self.years[0], self.years[1]):
            # Converts year number to 2-digit string
            year = '%02d' %year
            for month in xrange(self.months[0], self.months[1]):
                # Converts month number to 2-digit string
                month = '%02d' %month
        
                # Completes file name and opens file
                fname = self.ddir + year + '-' + month +'.nc'
                xr = xray.open_dataset(fname, decode_times=False)
                temp = self.function(xr, *self.params)
                out = np.append(out, temp)
                xr.close()
        return out
                
def extract_volume(xr, variable, ibounds = [0, 384], jbounds = [0, 320], kbounds = [0, 60]):
    """Obtain values of input variable in cells within specified boundaries"""
    try:
        variable = xr.variables[variable][:]
    except KeyError:
        pass
    except TypeError:
        pass
    try:
        volume_variable = variable[..., kbounds[0]:kbounds[1], ibounds[0]:ibounds[1], jbounds[0]:jbounds[1]]
    except IndexError:
        volume_variable = variable[..., ibounds[0]:ibounds[1], jbounds[0]:jbounds[1]]
    return volume_variable
                
def meanconcentration(xr, variable, ibounds, jbounds, kbounds):
    """Returns mean concentration variable for input file"""
    variable_integral = variable_volumeintegral(xr, variable, ibounds, jbounds, kbounds)
    total_volume = initial_volume(xr, variable, ibounds, jbounds, kbounds)
    mean_concentration = variable_integral / total_volume
    return mean_concentration

def variable_volumeintegral(xr, variable, ibounds, jbounds, kbounds):
    """Returns globally volume integrated variable for input month file"""
    dz = xr.variables['dz']
    variable = extract_volume(xr, variable, ibounds, jbounds, kbounds)
    tarea = extract_volume(xr, 'TAREA', ibounds, jbounds)
    
    variable_times_volume = variable * dz[kbounds[0]:kbounds[1]] * tarea
    variable_integral = variable_times_volume.sum()
    # Converts cm^3 to m^3
    variable_integral = variable_integral * 1e-6
    return variable_integral

def initial_volume(xr, varname, ibounds = [0, 384], jbounds = [0, 320], kbounds = [0, 60]):
    """Returns volume over which variable is defined"""
    dz = xr.variables['dz'][:]
    variable = extract_volume(xr, varname, ibounds, jbounds, kbounds)
    variable = variable[0]
    tarea = extract_volume(xr, 'TAREA', ibounds, jbounds)
    
    #nans = np.repeat('nan', np.prod(np.shape(variable)))
    #nans.shape = np.shape(variable)
    nans_bool = np.isnan(variable)
    
    initial_volume = np.ma.masked_array(
        dz[kbounds[0]:kbounds[1]] * tarea,
        nans_bool).sum()
    
    # Converts cm^3 to m^3
    initial_volume = initial_volume * 1e-6
    return initial_volume

def surface_integral(xr, varname, ibounds = [0, 384], jbounds = [0, 320], kbounds = [0, 60]):
    """Returns surface integral for variable defined at ocean surface"""
    # Converts variable and tarea to correct format
    tarea = xr.variables['TAREA'][:]
    variable = extract_volume(xr, varname, ibounds, jbounds)
    variable = variable[0]
    tarea = extract_volume(xr, tarea, ibounds, jbounds)
    
    # Computes surface integral
    variable_times_area = variable * tarea
    
    # Converts cm^2 to m^2
    variable_times_area = variable_times_area * 0.0001
    
    surface_integral = variable_times_area.sum()
    return surface_integral

def fwflux(xr, varname = "", ibounds = [0, 384], jbounds = [0, 320], kbounds = [0, 60]):
    """Returns integrated freshwater flux for input file"""
    qflux_integral = surface_integral(xr, 'QFLUX', ibounds, jbounds)
    frazil_ice = qflux_integral / 334000.0
    sfwf_integral = surface_integral(xr, 'SFWF', ibounds, jbounds)
    totalflux_integral = sfwf_integral - frazil_ice
    return totalflux_integral